export default function({ store, req }) {
  const isServer = !!process.server

  // If nuxt generate, pass this middleware
  if (isServer && !req) return

  store.dispatch('auth/checkAuthentication', { isServer, req })
}
