import { mapGetters, mapActions } from 'vuex'
import isEmpty from 'lodash/isEmpty'

const ProfileBasePath = '/profile'

export default {
  layout: 'whether',

  middleware: 'auth',

  fetch({ store }) {
    return store.dispatch('account/loadLoggedUserAccount')
  },

  data() {
    return {
      groupChatEmbedded: false,

      groupChatOptions: {
        chat_height: '600px',
        chat_width: '990px',
        chat_iframe: {}, // intentionally left blank
        chat_groupid: 'GROUP_1578418045_1592718625'
      },

      groupChatLoadingIssue: false,

      publicUrlPath: `${process.env.EnvURL}${ProfileBasePath}/`
    }
  },

  watch: {
    cometChatApiLoaded: 'handleChatApiLoading'
  },

  mounted() {
    if (this.isAccountLoaded) {
      this.loadCometChatApi(this.userData)
        .then(() => {
          this.embedGroupChat()
        })
        .catch(() => {
          this.groupChatLoadingIssue = true
        })
    }
  },

  computed: {
    ...mapGetters({
      loggedUser: 'auth/loggedUser',
      cometChatApiLoaded: 'misc/cometChatApiLoaded',
      accountPersonal: 'account/accountPersonal',
      accountProfileHighlights: 'account/accountProfileHighlights',
      isAccountLoaded: 'account/isAccountLoaded'
    }),

    groupChatIframeOptions() {
      return {
        module: 'synergy',
        width: '100%',
        height: '100%'
      }
    },

    userFullName() {
      return `${this.accountPersonal.firstName || ''} ${this.accountPersonal
        .lastName || ''}`.trim()
    },
    userProfilePublicUrl() {
      return !isEmpty(this.accountProfileHighlights) &&
        !isEmpty(this.accountProfileHighlights.publicProfileURL)
        ? `${this.publicUrlPath}${this.accountProfileHighlights.publicProfileURL}`
        : undefined
    },

    userData() {
      return {
        chat_name: this.userFullName,
        chat_id: String(this.accountPersonal.id),
        chat_avatar: this.accountPersonal.profileImageURL,
        chat_link: this.userProfilePublicUrl
      }
    }
  },

  methods: {
    ...mapActions({
      loadCometChatApi: 'misc/loadCometChatApi',
      embedCometGroupChat: 'misc/embedCometGroupChat'
    }),

    embedGroupChat() {
      if (!this.groupChatEmbedded) {
        this.embedCometGroupChat({
          groupId: this.groupChatOptions.chat_groupid,
          groupChatOptions: this.groupChatOptions,
          groupChatIframeOptions: this.groupChatIframeOptions
        })

        this.groupChatEmbedded = true
      }
    },

    handleChatApiLoading() {
      if (this.isAccountLoaded) {
        this.embedGroupChat()
      }
    }
  }
}
