export const UserRole = {
  JOBSEEKER: 'jobseeker',
  EMPLOYER: 'employer',
  ASSESSMENT_TAKER: 'assessment'
}

export const RoleType = {
  STUDENT_FULL: {
    id: 'student_full',
    name: 'Full-time Student'
  },
  PART_FULL: {
    id: 'student_part',
    name: 'Part-time Student'
  },
  EMPLOYER_FULL: {
    id: 'employer_full',
    name: 'Full-time Employee'
  },
  OTHER: {
    id: 'other',
    name: 'Other'
  }
}

export default {
  RoleType,
  UserRole
}
