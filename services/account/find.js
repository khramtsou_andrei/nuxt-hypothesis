import BWD from '@betterweekdays/discovery-sdk'
import first from 'lodash/first'

const failed = (error) => {
  console.log('BWDUsers.getMe: Failed to return successfully', error)
  return Promise.reject(error)
}

export default () => {
  return BWD.Users.getMe()
    .then((results) => first(results.data))
    .catch((error) => failed(error))
}
