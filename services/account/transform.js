import pick from 'lodash/pick'
import isEmpty from 'lodash/isEmpty'
import includes from 'lodash/includes'
import isObject from 'lodash/isObject'
import assign from 'lodash/assign'
import { UserRole, RoleType } from '~/services/misc/assessmentConstants'

export default (user) => {
  const transformedUser = assign({}, user)

  // default isStudent to true
  // if role is 'jobseeker' and the answer hasn't been set yes
  if (
    transformedUser.role === UserRole.JOBSEEKER &&
    isEmpty(transformedUser.ethnicity) &&
    isEmpty(transformedUser.company) &&
    isEmpty(transformedUser.unregisteredCompany)
  ) {
    transformedUser.isStudent = true
  }

  // TODO: eliminate as it is server responsibility
  // when roleType is 'Full-time student' or 'Part-time student'
  // and role is 'Assessment Taker', change role to 'Jobseeker'
  const jobseekerRoleTypes = [RoleType.STUDENT_FULL.id, RoleType.PART_FULL.id]
  if (
    transformedUser.role === UserRole.ASSESSMENT_TAKER &&
    includes(jobseekerRoleTypes, transformedUser.roleType)
  ) {
    transformedUser.role = UserRole.JOBSEEKER
  }

  // transform JSON to object
  if (
    !isEmpty(transformedUser.jsAppData) &&
    !isObject(transformedUser.jsAppData)
  ) {
    transformedUser.jsAppData = JSON.parse(transformedUser.jsAppData)
  }

  return {
    personal: pick(transformedUser, [
      'id',
      'firstName',
      'lastName',
      'userName',
      'mail',
      'gender',
      'phone',
      'phoneVerified',
      'linkedIn',
      'school',
      'unregisteredSchool',
      'graduationDate',
      'intercomHash',
      'casuallyBrowsing',
      'gpa',
      'completedOnBoardingStepOne',
      'completedOnBoardingStepTwo',
      'role',
      'isCoreg',
      'referralCode',
      'myReferralCode',
      'jsAppData',
      'isStudent',
      'roleType',
      'isFirstGeneration',
      'isLowIncome',
      'ethnicity',
      'company',
      'unregisteredCompany',
      'profTitle',
      'hasProfile',
      'profileImageURL',
      'demographicsGoal',
      'demographicsGoalFrequency',
      'isVeteran',
      'firebaseId',
      'mnUserId'
    ]),
    jobPreferences: {
      goal: transformedUser.goal,
      preferredCompanies: transformedUser.preferredCompanies || [],
      locations: transformedUser.locations || [],
      jobAreas: transformedUser.jobAreas || [],
      fieldsOfStudy: transformedUser.fieldsOfStudy || [],
      org: pick(transformedUser.org, [
        'id',
        'title',
        'inviteCode',
        'assessmentOptional'
      ]),
      courses: transformedUser.courses || [],
      onets: transformedUser.onets || [],
      desiredCompetencies: transformedUser.desiredCompetency || [],
      degreePrograms: transformedUser.degreePrograms || [],
      viewJobsAdviceFeed: transformedUser.viewJobsAdviceFeed
    },
    resumes: {
      primaryResumes: transformedUser.resume
    },
    assessmentHighlights: pick(transformedUser.assessmentInfo, [
      'assessmentId',
      'assessmentIsComplete',
      'assessmentIsReady',
      'assessmentPrice',
      'canTakeAssessment',
      'assessmentPaymentSource'
    ])
  }
}
