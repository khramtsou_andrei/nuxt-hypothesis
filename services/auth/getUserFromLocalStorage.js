export default () => {
  const json = window.localStorage.user
  return json ? JSON.parse(json) : undefined
}
