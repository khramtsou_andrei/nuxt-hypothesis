import jwtDecode from 'jwt-decode'

export default (req) => {
  if (!req.headers.cookie) return

  const jwtCookie = req.headers.cookie
    .split(';')
    .find((c) => c.trim().startsWith('jwt='))
  if (!jwtCookie) return

  const jwt = jwtCookie.split('=')[1]
  return jwtDecode(jwt)
}
