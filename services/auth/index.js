import getUserFromCookie from './getUserFromCookie'
import getUserFromLocalStorage from './getUserFromLocalStorage'

export default {
  getUserFromCookie,
  getUserFromLocalStorage
}
