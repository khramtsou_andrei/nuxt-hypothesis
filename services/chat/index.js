import assign from 'lodash/assign'
import isFunction from 'lodash/isFunction'
import keys from 'lodash/keys'
import isEmpty from 'lodash/isEmpty'

export default {
  appId: process.env.CometChatAppID,
  authKey: process.env.CometChatAuthKey,

  apiLoaded: null,

  load(userData) {
    // user management
    this.initUserData(userData)

    if (!this.apiLoaded) {
      this.apiLoaded = new Promise((resolve, reject) => {
        const script = document.createElement('script')
        script.type = 'text/javascript'
        script.src = `https://fast.cometondemand.net/${this.appId}x_xchatx_xcorex_xembedcode.js`
        script.onload = () => {
          resolve()
        }
        script.onerror = (error) => {
          reject(error)
        }

        document.body.appendChild(script)
      })
    }

    return this.apiLoaded
  },

  initUserData(userData) {
    const data = assign({}, userData, {
      chat_appid: this.appId,
      chat_auth: this.authKey
    })
    keys(data).forEach((name) => {
      window[name] = data[name]
    })
  },

  embedChat(options = {}, iframeOptions = {}) {
    // initialize general chat options
    // TODO: add Advanced Security options
    keys(options).forEach((name) => {
      window[name] = options[name]
    })

    // initialize chat iframe options
    window.chat_iframe = assign({}, window.chat_iframe, iframeOptions)

    if (isFunction(window.addEmbedIframe)) {
      window.addEmbedIframe(window.chat_iframe)
    }
  },

  embedGroupChat(guid, options = {}, iframeOptions = {}) {
    const extendedIframeOptions = assign({}, iframeOptions, {
      src: `https://${this.appId}.cometondemand.net/cometchat_embedded.php?guid=${guid}`
    })

    this.embedChat(options, extendedIframeOptions)
  },

  embedOneToOneChat(uid, options = {}, iframeOptions = {}) {
    const extendedIframeOptions = assign({}, iframeOptions, {
      src: !isEmpty(uid)
        ? `https://${this.appId}.cometondemand.net/cometchat_embedded.php?uid=${uid}`
        : `https://${this.appId}.cometondemand.net/cometchat_embedded.php`
    })

    this.embedChat(options, extendedIframeOptions)
  },
  launchOneToOneChat(uid) {
    window.jqcc.cometchat.launch({ uid })
  }
}
