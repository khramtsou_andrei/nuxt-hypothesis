import CometChat from '@@/services/chat'
import { COMET_CHAT_API_LOADED } from './mutation-types'

export default {
  loadCometChatApi({ commit }, userData) {
    return CometChat.load(userData)
      .then(() => {
        commit(COMET_CHAT_API_LOADED)
      })
      .catch((error) => {
        console.log(
          'An error occurred when loading Comet Chat API. Error: ',
          error
        )
        return Promise.reject(error)
      })
  },
  embedCometGroupChat(
    context,
    { groupId, groupChatOptions, groupChatIframeOptions }
  ) {
    CometChat.embedGroupChat(groupId, groupChatOptions, groupChatIframeOptions)
  },
  embedCometOneToOneChat(context, { userId, chatOptions, chatIframeOptions }) {
    CometChat.embedOneToOneChat(userId, chatOptions, chatIframeOptions)
  },
  launchCometOneToOneChat(context, { userId }) {
    CometChat.launchOneToOneChat(userId)
  }
}
