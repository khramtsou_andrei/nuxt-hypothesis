import { COMET_CHAT_API_LOADED } from './mutation-types'

export default {
  [COMET_CHAT_API_LOADED](state) {
    state.cometChatApiLoaded = true
  }
}
