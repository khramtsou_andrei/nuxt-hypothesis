import { UserRole } from '~/services/misc/assessmentConstants'

export default () => ({
  user: {
    personal: {
      id: null,
      userName: null,
      firstName: null,
      lastName: null,
      mail: null,
      gender: null,
      phone: null,
      phoneVerified: null,
      school: null,
      unregisteredSchool: null,
      linkedIn: null,
      graduationDate: null,
      intercomHash: null,
      casuallyBrowsing: null,
      gpa: null,
      completedOnBoardingStepOne: null,
      completedOnBoardingStepTwo: null,
      role: UserRole.JOBSEEKER,
      isCoreg: null,
      referralCode: null,
      myReferralCode: null,
      jsAppData: null,
      isStudent: null,
      roleType: null,
      isFirstGeneration: null,
      isLowIncome: null,
      ethnicity: null,
      company: null,
      unregisteredCompany: null,
      profTitle: null,
      hasProfile: null,
      profileImageURL: null,
      demographicsGoal: null,
      demographicsGoalFrequency: null,
      isVeteran: null,
      firebaseId: null,
      mnUserId: null
    },

    jobPreferences: {
      goal: null,
      preferredCompanies: [],
      locations: [],
      jobAreas: [],
      fieldsOfStudy: [],
      courses: [],
      onets: [],
      org: {
        id: null,
        title: null,
        inviteCode: null,
        assessmentOptional: false
      },
      desiredCompetencies: [],
      degreePrograms: [],
      viewJobsAdviceFeed: null
    },

    resumes: {
      primaryResumes: []
    },

    assessmentHighlights: {
      assessmentId: null,
      assessmentIsComplete: null,
      assessmentIsReady: null,
      assessmentPrice: null,
      canTakeAssessment: null,
      assessmentPaymentSource: null
    }
  },

  loadingStatus: false
})
