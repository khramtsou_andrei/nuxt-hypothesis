import isEmpty from 'lodash/isEmpty'

export const user = (state) => state.user
export const accountLoadingStatus = (state) => state.loadingStatus
export const isAccountLoaded = (state) => !isEmpty(state.user.personal.userName)
export const accountJobPreferences = (state) => state.user.jobPreferences
export const accountPersonal = (state) => state.user.personal
export const accountHasProfile = (state) =>
  !isEmpty(state.user.personal.hasProfile)
export const accountProfileHighlights = (state) =>
  state.user.personal.hasProfile

export default {
  user,
  accountLoadingStatus,
  isAccountLoaded,
  accountJobPreferences,
  accountPersonal,
  accountHasProfile,
  accountProfileHighlights
}
