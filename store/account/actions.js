import {
  ACCOUNT_LOADING_STATUS_UPDATED,
  ACCOUNT_LOADED
} from './mutation-types'
import accountService from '~/services/account'

export default {
  setAccountLoading({ commit }, payload) {
    commit(ACCOUNT_LOADING_STATUS_UPDATED, payload)
  },

  loadLoggedUserAccount({ commit, dispatch }) {
    return accountService
      .find()
      .then((user) => {
        // format user data
        const transformedUser = accountService.transform(user)

        commit(ACCOUNT_LOADED, transformedUser)
      })
      .catch((error) => {
        return Promise.reject(error)
      })
  }
}
