import assign from 'lodash/assign'
import {
  ACCOUNT_LOADING_STATUS_UPDATED,
  ACCOUNT_LOADED
} from './mutation-types'

export default {
  [ACCOUNT_LOADING_STATUS_UPDATED](state, status) {
    state.loadingStatus = status
  },

  [ACCOUNT_LOADED](state, user) {
    state.user = assign({}, state.user, user)
  }
}
