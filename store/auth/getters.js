export const userAuthenticated = (state) => state.authenticated
export const loggedUser = (state) => state.user

export default {
  userAuthenticated,
  loggedUser
}
