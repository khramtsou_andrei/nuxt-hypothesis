import BWD from '@betterweekdays/discovery-sdk'
import {
  AUTHENTICATION_STATUS_UPDATED,
  AUTHENTICATION_USER_INDENTIFIED
} from './mutation-types'
import authService from '~/services/auth'

export default {
  checkAuthentication({ commit }, { isServer, req }) {
    BWD.Config.set({
      APIEnv: 'test',
      BackendEnv: 'stage'
    })

    const loggedUser = isServer
      ? authService.getUserFromCookie(req)
      : authService.getUserFromLocalStorage()

    if (loggedUser) {
      // Consider token expired 60 seconds early in case of clock sync issues.
      const expiresIn = loggedUser.exp - loggedUser.iat
      const expires = Date.now() + (expiresIn - 60) * 1000
      BWD.Config.set({
        AccessToken: {
          id_token:
            'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ik5VVkJSRVExUXpVNE16WkJPVFkzTmpreFJURTVPVUZFTTBOQk1qTkZNRFF3TnpaRk0wWTVNUSJ9.eyJodHRwczovL3d3dy50aGV3aGV0aGVyLmNvbS9yZWdpc3Rlci9tZXRob2QiOiIiLCJlbWFpbCI6InN0dWRlbnRAYncuY29tIiwiaXNzIjoiaHR0cHM6Ly9iZXR0ZXJ3ZWVrZGF5cy1zdGFnZS5hdXRoMC5jb20vIiwic3ViIjoiYXV0aDB8c3R1ZGVudEBidy5jb20iLCJhdWQiOiJ5UVJnVGtheUFsY2dERmNuaWNYQTk0dXA4Zi1ZejJTaiIsImlhdCI6MTU3OTE2NzcxMCwiZXhwIjoxNTc5MTY4NjEwfQ.CW-XwIw_EW0uY1cpAk7KXrD7SiRzU0W8TioF9CvIkCwJVTlqV-IoGm1GDRySMtTc0NHKR3Y9z__ltOvmUlgL3KvRyIWWi9e-5zmqcBrg228R7InvYDVhddMLgz-xij6P9W7bwS1TKkrIe7zAZA7tbRM3Ja5zGpugy8pcMtLQk6_OS9JDVs5fdTZpZBALRuIJ40StHtWR9ZY6W4yRxrLkVHXA4k8Zk7E0rr2nxXLPhHv56PRArqN6PlwErNT7CNnvmPr6rm0ijZb002LN21CfY7_5DWMdTKxgbs_JC9ZSXOOl0p0x1hSBy4Fx_O4MzyxMZEcIhbHHzYqsTgLSJaY2RA'
        },
        Auth0TokenExpires: new Date(expires)
      })

      commit(AUTHENTICATION_USER_INDENTIFIED, loggedUser)
    }

    const authStatus = !!loggedUser
    commit(AUTHENTICATION_STATUS_UPDATED, authStatus)
  }
}
