import assign from 'lodash/assign'

import {
  AUTHENTICATION_STATUS_UPDATED,
  AUTHENTICATION_USER_INDENTIFIED
} from './mutation-types'

export default {
  [AUTHENTICATION_STATUS_UPDATED](state, status) {
    state.authenticated = status
  },

  [AUTHENTICATION_USER_INDENTIFIED](state, user) {
    state.user = assign({}, state.user, user)
  }
}
