import state from './misc/state'
import getters from './misc/getters'
import mutations from './misc/mutations'
import actions from './misc/actions'

export default {
  mutations,
  state,
  actions,
  getters
}
